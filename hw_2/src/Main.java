import java.util.ArrayList;
import java.util.List;
import Geometry.Circular.Circle;
import Geometry.Angular.Rectangle;
import Geometry.Angular.Polygon;
import Geometry.Angular.Rhombus;

public class Main {
    public static void main(String[] args) {
        System.out.println("\nСоздаю круг радиусом 1 и центром в точке (10;0) черного цвета:");
        Circle circle1 = new Circle(10,0,1, "black");
        System.out.println(circle1);
        System.out.println("\nУстанавливую новые параметры круга и вывожу информацию о нем:");
        circle1.setRadius(10);
        circle1.setCenterX(0);
        circle1.setCenterY(0);
        circle1.setColor("white");
        System.out.println(circle1);
        System.out.println("\nВывожу только цвет круга:");
        System.out.println(circle1.getColor());
        System.out.println("\nВывожу периметр и площадь:");
        System.out.println(circle1.getPerimeter());
        System.out.println(circle1.getArea());

        System.out.println("\nСоздаю прямоугольник со сторонами 10, 5 и центром в точке (-1;-10) розового цвета:");
        Rectangle rectangle1 = new Rectangle(10,5,-1,-10,"pink");
        System.out.println(rectangle1);
        System.out.println("\nУстанавливую новые параметры прямоугольника и вывожу информацию о нем:");
        rectangle1.setCenterX(5);
        rectangle1.setCenterY(6);
        rectangle1.setSizeA(40);
        rectangle1.setSizeB(20);
        rectangle1.setColor("green");
        System.out.println(rectangle1);
        System.out.println("\nВывожу только цвет прямоугольнка:");
        System.out.println(rectangle1.getColor());
        System.out.println("\nВывожу периметр и площадь:");
        System.out.println(rectangle1.getPerimeter());
        System.out.println(rectangle1.getArea());
        System.out.println("\nВывожу его координаты:");
        rectangle1.output();

        System.out.println("\nСоздаю полигон с точками (1;1), (1;3), (3;3), (3;1) желтого цвета:");
        List<Double> coordinates= new ArrayList<>();
        coordinates.add(1.0);
        coordinates.add(1.0);
        coordinates.add(1.0);
        coordinates.add(3.0);
        coordinates.add(3.0);
        coordinates.add(3.0);
        coordinates.add(3.0);
        coordinates.add(1.0);
        Polygon polygon1 = new Polygon(coordinates,"yellow");
        System.out.println(polygon1);
        System.out.println("\nВывожу периметр и площадь:");
        System.out.println(polygon1.getPerimeter());
        System.out.println(polygon1.getArea());
        System.out.println("\nЗадаю другой цвет полигона -  красный:");
        polygon1.setColor("red");
        System.out.println(polygon1);
        System.out.println("\nВывожу только цвет полигона:");
        System.out.println(polygon1.getColor());
        System.out.println("\nВывожу его координаты:");
        polygon1.output();

        System.out.println("\nСоздаю ромб с длиной стороны 4 и длиной малой диагонали 4 с центром в точке (-1;-1) желтого цвета:");
        Rhombus rhombus1 = new Rhombus(5,6,-1,-1,"yellow");
        System.out.println(rhombus1);
        System.out.println("\nВывожу периметр и площадь через сторону и длину малой диагонали:");
        System.out.println(rhombus1.getPerimeterNew());
        System.out.println(rhombus1.getAreaNew());
        System.out.println("\nВывожу периметр и площадь через координаты, полученные из длины стороны, малой диагонали и координат центра:");
        System.out.println(rhombus1.getPerimeter());
        System.out.println(rhombus1.getArea());
        System.out.println("\nЗадаю другой цвет ромба - белый:");
        rhombus1.setColor("white");
        System.out.println(rhombus1);
        System.out.println("\nВывожу только цвет ромба:");
        System.out.println(rhombus1.getColor());
        System.out.println("\nВывожу его координаты:");
        rhombus1.output();
    }

}