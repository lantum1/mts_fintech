package Geometry.Circular;

import Geometry.GeomFigure;
import Geometry.Point;

public class Circle extends GeomFigure {
    private double radius;
    private Point centerCoord;
    private String color;
    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color = color;
    }
    public Circle(double centerX, double centerY, double radius, String color){
        this.centerCoord =new Point(centerX, centerY);
        this.radius=radius;
        this.color=color;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setCenterX(double centerX) {
        this.centerCoord.setX(centerX);
    }

    public void setCenterY(double centerY) {
        this.centerCoord.setY(centerY);
    }

    @Override
    public double getPerimeter(){
        return 2*Math.PI*radius;
    }
    @Override
    public double getArea(){
        return Math.PI*Math.pow(radius,2);
    }
    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", centerX=" + centerCoord.getX() +
                ", centerY=" + centerCoord.getY() +
                ", color='" + color + '\'' +
                '}';
    }
}
