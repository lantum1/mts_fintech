package Geometry.Angular;
import Geometry.GeomFigure;
import Geometry.Point;
import java.util.ArrayList;
import java.util.List;

public class Polygon extends GeomFigure implements WithAngles{
    private int count = 0;
    private List<Point> coordList =new ArrayList<>();
    private String color;
    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color = color;
    }
    protected List<Point> getCoordList() {
        return coordList;
    }

    protected void setCount(int count) {
        this.count = count;
    }

    public Polygon(String color){
        this.color=color;
    }
    public Polygon(List<Double> newList, String color) {
        for (int i=0; i<newList.size(); i+=2){
            Point tempPoint= new Point(newList.get(i), newList.get(i+1));
            coordList.add(tempPoint);
        }
        this.count=this.coordList.size();
        this.color=color;
    }
    @Override
    public double getPerimeter() {
        double perimeter=0;
        for (int i=0; i<count; i++){
            double xII1=Math.abs(coordList.get(i).getX()- coordList.get((i+1)%count).getX());
            double yII1=Math.abs(coordList.get(i).getY()- coordList.get((i+1)%count).getY());
            double length=Math.sqrt(Math.pow(xII1,2)+Math.pow(yII1,2));
            perimeter+=length;
        }
        return perimeter;
    }

    @Override
    public double getArea() {
        double tempSum1=0;
        double tempSum2=0;
        for (int i=0; i<count; i++){
            tempSum1+= coordList.get(i).getX()* coordList.get((i+1)%count).getY();
            tempSum2+= coordList.get((i+1)%count).getX()* coordList.get(i).getY();
        }
        return 0.5*Math.abs(tempSum1-tempSum2);
    }
    @Override
    public String toString() {
        return "Polygon{" +
                "count=" + count +
                ", color='" + color + '\'' +
                '}';
    }
    @Override
    public void output(){
        for (int i = 0; i < count; i++){
            System.out.println(Double.toString(getCoordList().get(i).getX()) + " " + Double.toString(getCoordList().get(i).getY()));
        }
    }
}
