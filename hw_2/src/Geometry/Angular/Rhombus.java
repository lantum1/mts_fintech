package Geometry.Angular;
import Geometry.Point;
public class Rhombus extends Polygon implements Tetragon{
    private double size;
    private double smallDiagonal;
    private double bigDiagonal;
    private double centerX;
    private double centerY;
    public Rhombus(double size, double smallDiagonal, double centerX, double centerY, String color){
        super(color);
        this.size=size;
        this.smallDiagonal = smallDiagonal;
        this.bigDiagonal =Math.sqrt(4*Math.pow(size,2)-Math.pow(smallDiagonal,2));
        this.centerX = centerX;
        this.centerY = centerY;
        getCoordList().add(new Point(centerX - smallDiagonal /2, centerY));
        getCoordList().add(new Point(centerX, centerY + bigDiagonal /2));
        getCoordList().add(new Point(centerX + smallDiagonal /2, centerY));
        getCoordList().add(new Point(centerX, centerY - bigDiagonal /2));
        setCount(angleCount);
    }
    public double getPerimeterNew(){
        return size* angleCount;
    }
    public double getAreaNew(){
        return 0.5* bigDiagonal * smallDiagonal;
    }
    @Override
    public String toString() {
        return "Rhombus{" +
                "size=" + size +
                ", smallDiagonal=" + smallDiagonal +
                ", centerX=" + centerX +
                ", centerY=" + centerY +
                ", color='" + getColor() + '\'' +
                '}';
    }
}
