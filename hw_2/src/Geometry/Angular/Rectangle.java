package Geometry.Angular;
import Geometry.GeomFigure;
import Geometry.Point;
public class Rectangle extends GeomFigure implements WithAngles{
    private double sizeA;
    private double sizeB;
    private Point centerCoord;
    private String color;
    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color = color;
    }
    public Rectangle(double sizeA, double sizeB, double centerX, double centerY, String color){
        this.sizeA = sizeA;
        this.sizeB = sizeB;
        this.centerCoord =new Point(centerX, centerY);
        this.color=color;
    }
    public void setSizeA(double sizeA) {
        this.sizeA = sizeA;
    }

    public void setSizeB(double sizeB) {
        this.sizeB = sizeB;
    }

    public void setCenterX(double centerX) {
        this.centerCoord.setX(centerX);
    }

    public void setCenterY(double centerY) {
        this.centerCoord.setY(centerY);
    }

    @Override
    public double getPerimeter(){
        return (sizeA + sizeB)*2;
    }
    @Override
    public double getArea(){
        return (sizeA * sizeB);
    }
    @Override
    public String toString() {
        return "Rectangle{" +
                "sizeA=" + sizeA +
                ", sizeB=" + sizeB +
                ", centerX=" + centerCoord.getX() +
                ", centerY=" + centerCoord.getY() +
                ", color='" + color + '\'' +
                '}';
    }

    @Override
    public void output() {
        System.out.println(Double.toString(centerCoord.getX()- sizeB /2) + " " + Double.toString(centerCoord.getY()+ sizeA /2));
        System.out.println(Double.toString(centerCoord.getX()+ sizeB /2) + " " + Double.toString(centerCoord.getY()+ sizeA /2));
        System.out.println(Double.toString(centerCoord.getX()+ sizeB /2) + " " + Double.toString(centerCoord.getY()- sizeA /2));
        System.out.println(Double.toString(centerCoord.getX()- sizeB /2) + " " + Double.toString(centerCoord.getY()- sizeA /2));
    }
}
